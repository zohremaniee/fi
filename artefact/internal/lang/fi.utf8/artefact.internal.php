<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['Created'] = 'Luotu';

$string['Description'] = 'Kuvaus';

$string['Download'] = 'Lataa';

$string['Owner'] = 'Omistaja';

$string['Preview'] = 'Esikatselu';

$string['Size'] = 'Koko';

$string['Title'] = 'Otsikko';

$string['Type'] = 'Tyyppi';

$string['aboutdescription'] = 'Kirjoita etu- ja sukunimesi tähän. Voit esiintyä palvelussa myös nimimerkillä.';

$string['aboutme'] = 'Tietoja minusta';

$string['addbutton'] = 'Lisää';

$string['address'] = 'Katuosoite';

$string['aimscreenname'] = 'AIM-nimi';

$string['blogaddress'] = 'Blogin osoite';

$string['businessnumber'] = 'Työpuhelin';

$string['city'] = 'Kaupunki/Alue';

$string['contact'] = 'Yhteystiedot';

$string['country'] = 'Maa';

$string['email'] = 'Sähköpostiosoite';

$string['emailactivation'] = 'Sähköpostiosoiteen aktivoiminen';

$string['emailactivationdeclined'] = 'Email Activation Declined Successfully' /** MISSING **/ ;

$string['emailactivationfailed'] = 'Sähköpostiosoitteen aktivoiminen epäonnistui';
+$string['emailactivationdeclined'] = 'Sähköpostiosoitteen aktivoiminen peruutettu';

$string['emailactivationsucceeded'] = 'Sähköpostiosoite aktivoitu';

$string['emailaddress'] = 'Vaihtoehtoinen sähköpostiosoite';

$string['emailalreadyactivated'] = 'Sähköpostiosoite on jo aktivoitu';

$string['emailingfailed'] = 'Profiili on tallennettu, mutta sähköposteja ei ole lähetetty: %s';

$string['emailvalidation_body'] = <<<EOF
Hei %s,
 
Sähköpostiosoite %s on lisätty käyttäjätiliisi Maharassa. Ole hyvä ja klikkaa alla olevaa linkkiä aktivoidaksesi tämän osoitteen.

%s

Jos et ole pyytänyt tämän osoitteen lisäämistä käyttäjätiliisi, klikkaa alla olevaa linkkiä peruuttaaksesi aktivoinnin.
 
%s
EOF;

$string['emailvalidation_subject'] = 'Sähköpostiosoitteen vahvistaminen';

$string['faxnumber'] = 'Faksinumero';

$string['firstname'] = 'Etunimi';

$string['fullname'] = 'Koko nimi';

$string['general'] = 'Yleinen';

$string['homenumber'] = 'Kotipuhelin';

$string['icqnumber'] = 'ICQ-numero';

$string['industry'] = 'Ammattiala';

$string['infoisprivate'] = 'Nämä tiedot ovat yksityisiä, ellet jaa niitä profiilisivullasi tai portfoliosivuilla.';

$string['institution'] = 'Instituutio';

$string['introduction'] = 'Esittely';

$string['invalidemailaddress'] = 'Epäkelpo sähköpostiosoite';

$string['jabberusername'] = 'Jabberin käyttäjätunnus';

$string['lastmodified'] = 'Viimeksi muokattu';

$string['lastname'] = 'Sukunimi';

$string['loseyourchanges'] = 'Menetät muutokset?';

$string['maildisabled'] = 'Sähköpostin lähetys pois käytöstä';

$string['mandatory'] = 'Pakollinen';

$string['messaging'] = 'Viestintä';

$string['mobilenumber'] = 'Matkapuhelin';

$string['msnnumber'] = 'MSN Chat';

$string['name'] = 'Nimi';

$string['occupation'] = 'Ammatti';

$string['officialwebsite'] = 'Virallinen www-osoite';

$string['personalwebsite'] = 'Henkilökohtainen www-osoite';

$string['pluginname'] = 'Profiili';

$string['preferredname'] = 'Nimimerkki';

$string['principalemailaddress'] = 'Ensisijainen sähköpostiosoite';

$string['profile'] = 'Profiili';

$string['profilefailedsaved'] = 'Profiilin tallennus epäonnistui';

$string['profileinformation'] = 'Profiilitiedot';

$string['profilepage'] = 'Profiilisivu';

$string['profilesaved'] = 'Profiili tallennettu';

$string['public'] = 'Julkinen';

$string['saveprofile'] = 'Tallenna profiili';

$string['skypeusername'] = 'Skypen käyttäjätunnus';

$string['studentid'] = 'Opiskelija ID';

$string['town'] = 'Kaupunki';

$string['unvalidatedemailalreadytaken'] = 'Sähköpostiosoite, jota yrität vahvistaa, on jo käytössä';

$string['validationemailsent'] = 'vahvistussähköposti on lähetetty';

$string['validationemailwillbesent'] = 'vahvistussähköposti lähetetään kun tallennat profiilisi';

$string['verificationlinkexpired'] = 'Vahvistuslinkki erääntynyt';

$string['viewallprofileinformation'] = 'Näytä kaikki profiilitieto';

$string['viewmyprofile'] = 'Näytä profiilini';

$string['viewprofilepage'] = 'Näytä profiilisivu';

$string['yahoochat'] = 'Yahoo Chat';

