<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['addblog'] = 'Luo blogi';

$string['addone'] = 'Kirjoita merkintä!';

$string['addpost'] = 'Uusi blogimerkintä';

$string['alignment'] = 'Kohdistus';

$string['allowcommentsonpost'] = 'Salli kommentit.';

$string['allposts'] = 'Kaikki merkinnät';

$string['alt'] = 'Kuvaus';

$string['attach'] = 'Liitä';

$string['attachedfilelistloaded'] = 'Liitetiedostolista ladattu';

$string['attachedfiles'] = 'Liitetiedostot';

$string['attachment'] = 'Liite';

$string['attachments'] = 'Liitteet';

$string['baseline'] = 'Alarivi';

$string['blog'] = 'Blogi';

$string['blogcopiedfromanotherview'] = 'Huomio: Tämä alue on kopioitu toisesta sivusta. Voit siirtää tai poistaa sen, mutta et voi muuttaa %s sisältöä.';

$string['blogdeleted'] = 'Blogi poistettu';

$string['blogdesc'] = 'Kuvaus';

$string['blogdescdesc'] = 'Esim. "Kirjauksia Minnan kokemuksista ja pohdinnoista"';

$string['blogdoesnotexist'] = 'Yrität lukea blogia, jota ei ole olemassa';

$string['blogpost'] = 'Blogimerkintä';

$string['blogpostdeleted'] = 'Blogimerkintä poistettu';

$string['blogpostdoesnotexist'] = 'Yrität lukea blogimerkintää, jota ei ole olemassa';

$string['blogpostpublished'] = 'Blogimerkintä julkaistu';

$string['blogpostsaved'] = 'Merkintä tallennettu';

$string['blogs'] = 'Blogit';

$string['blogsettings'] = 'Blogin asetukset';

$string['blogtitle'] = 'Otsikko';

$string['blogtitledesc'] = 'Esim. "Minnan työharjoittelu vanhainkodissa"';

$string['border'] = 'Reuna';

$string['bottom'] = 'Alaosa';

$string['cancel'] = 'Peruuta';

$string['cannotdeleteblogpost'] = 'Merkinnän poistamisen yhteydessä tapahtui virhe';

$string['copyfull'] = 'Muut käyttäjät saavat oman kopion kohteesta %s';

$string['copynocopy'] = 'Sivun kopioinnin yhteydessä älä kopioi aluetta';

$string['copyreference'] = 'Muut käyttäjät saavat näyttää sivullaan kohteen %s';

$string['createandpublishdesc'] = 'Tämä toiminto luo blogimerkinnän ja julkaisee sen.';

$string['createasdraftdesc'] = 'Tämä toiminto luo blogimerkinnän, mutta sitä ei näytetä muille ennen kuin julkaiset sen.';

$string['createblog'] = 'Luo blogi';

$string['dataimportedfrom'] = 'Data tuotu kohteesta %s';

$string['defaultblogtitle'] = 'Käyttäjän %s blogi';

$string['delete'] = 'Poista';

$string['deleteblog?'] = 'Oletko varma, että haluat poistaa tämän blogin?';

$string['deleteblogpost?'] = 'Oletko varma, että haluat poistaa tämän merkinnän?';

$string['description'] = 'Kuvaus';

$string['dimensions'] = 'Mittasuhteet';

$string['draft'] = 'Luonnos';

$string['edit'] = 'Muokkaa';

$string['editblogpost'] = 'Muokkaa blogimerkintää';

$string['enablemultipleblogstext'] = 'Sinulla on yksi blogi. Jos haluat aloittaa toisen, ota käyttöön useat blogit <a href="%saccount/">asetussivulla</a>.';

$string['entriesimportedfromleapexport'] = 'LEAP-tiedostosta tuodut merkinnät, joita ei voitu sijoittaa muualle';

$string['errorsavingattachments'] = 'Blogimerkinnän liiteiden tallennuksessa tapahtui virhe';

$string['feedrights'] = 'Copyright %s.';

$string['feedsnotavailable'] = 'Syötteet eivät ole käytössä tälle artefaktityypille.';

$string['horizontalspace'] = 'Tila vakaasuunnassa';

$string['image_list'] = 'Liitetty kuva';

$string['insert'] = 'Lisää';

$string['insertimage'] = 'Lisää kuva';

$string['left'] = 'Vasen';

$string['middle'] = 'Keskiosa';

$string['moreoptions'] = 'Muut asetukset';

$string['mustspecifycontent'] = 'Merkintä ei voi olla tyhjä';

$string['mustspecifytitle'] = 'Merkinnän otsikko ei voi olla tyhjä';

$string['name'] = 'Nimi';

$string['newattachmentsexceedquota'] = 'Tähän blogimerkintään lataamiesi uusien tiedostojen kokonaiskoko ylittää kiintiösi. Voit mahdollisesti tallentaa merkintäsi, jos poistat joitakin liitteitä jotka juuri lisäsit.';

$string['newblog'] = 'Uusi blogi';

$string['newblogpost'] = 'Uusi blogimerkintä blogissa "%s"';

$string['newerposts'] = 'Uudemmat merkinnät';

$string['nofilesattachedtothispost'] = 'Ei liitetiedostoja';

$string['noimageshavebeenattachedtothispost'] = 'Kuvia ei ole liitetty tähän merkintään. Sinun tulisi tuoda tai liittää kuvan merkintään ennen kuin voit upottaa sen merkinnän sisään ???';

$string['nopostsyet'] = 'Ei vielä merkintöjä.';

$string['noresults'] = 'Ei blogimerkintöjä';

$string['olderposts'] = 'Vanhemmat merkintöjä';

$string['pluginname'] = 'Blogit';

$string['post'] = 'entry' /** MISSING **/ ;

$string['postbody'] = 'Tekstikenttä';

$string['postbodydesc'] = ' ' /** MISSING **/ ;

$string['postedbyon'] = '%s on tehnyt merkinnän %s';

$string['postedon'] = 'Luotu';

$string['posts'] = 'merkintää';

$string['postscopiedfromview'] = 'Merkinnät kopioitu sivulta %s';

$string['posttitle'] = 'Otsikko';

$string['publish'] = 'Julkaise';

$string['publishblogpost?'] = 'Oletko varma, että haluat julkaista tämän merkinnän?';

$string['published'] = 'Julkaistu';

$string['publishfailed'] = 'Tapahtui virhe. Merkintääsi ei julkaistu';

$string['remove'] = 'Poista';

$string['right'] = 'Oikea';

$string['save'] = 'Tallenna';

$string['saveandpublish'] = 'Tallenna ja julkaise';

$string['saveasdraft'] = 'Tallenna luonnoksena';

$string['savepost'] = 'Tallenna merkintä';

$string['savesettings'] = 'Tallenna asetukset';

$string['settings'] = 'Asetukset';

$string['src'] = 'Kuvan osoite';

$string['textbottom'] = 'Tekstin alaosa';

$string['texttop'] = 'Tekstin yläosa';

$string['thisisdraft'] = 'Tämä merkintä on luonnos';

$string['thisisdraftdesc'] = 'Luonnokset näytetään ainoastaan blogin omistajalle.';

$string['title'] = 'Otsikko';

$string['top'] = 'Yläosa';

$string['update'] = 'Päivitä';

$string['verticalspace'] = 'Tila pystysuunnassa';

$string['viewblog'] = 'Näytä blogi';

$string['viewposts'] = 'Kopioidut merkinnät (%s)';

$string['youarenottheownerofthisblog'] = 'Et ole tämän blogin omistaja';

$string['youarenottheownerofthisblogpost'] = 'Et ole tämän blogimerkinnän omistaja';

$string['youhaveblogs'] = 'Sinulla on %s blogia.';

$string['youhavenoblogs'] = 'Sinulla ei ole blogeja.';

$string['youhaveoneblog'] = 'Sinulla on yksi blogi.';

