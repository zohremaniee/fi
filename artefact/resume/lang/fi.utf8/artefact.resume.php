<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['academicgoal'] = 'Koulutustavoitteet';

$string['academicskill'] = 'Koulutuksen kautta saadut taidot ja osaaminen';

$string['achievements'] = 'Saavutukset';

$string['book'] = 'Kirjat ja julkaisut';

$string['careergoal'] = 'Uratavoitteet';

$string['certification'] = 'Sertifikaatit, suositukset ja palkinnot';

$string['citizenship'] = 'Kansalaisuus';

$string['compositedeleteconfirm'] = 'Haluatko varmasti poistaa tämän?';

$string['compositedeleted'] = 'Poistettu';

$string['compositesaved'] = 'Tallennettu';

$string['compositesavefailed'] = 'Tallennus epäonnistui';

$string['contactinformation'] = 'Yhteystiedot';

$string['contribution'] = 'Oma osuuteni';

$string['coverletter'] = 'Saatekirje';

$string['current'] = 'Nykyinen';

$string['date'] = 'Päivämäärä';

$string['dateofbirth'] = 'Syntymäaika';

$string['defaultacademicgoal'] = '';

$string['defaultcareergoal'] = '';

$string['defaultpersonalgoal'] = '';

$string['description'] = 'Kuvaus';

$string['detailsofyourcontribution'] = 'Tarkempi kuvaus julkaisusta';

$string['educationandemployment'] = 'Työ & koulutus';

$string['educationhistory'] = 'Koulutus';

$string['employer'] = 'Työnantaja';

$string['employeraddress'] = 'Työnantajan osoite';

$string['employmenthistory'] = 'Työkokemus';

$string['enddate'] = 'Päättymispäivä';

$string['female'] = 'Nainen';

$string['gender'] = 'Sukupuoli';

$string['goalandskillsaved'] = 'Tallennettu';

$string['goals'] = 'Tavoitteet';

$string['institution'] = 'Instituutio';

$string['institutionaddress'] = 'Instituution osoite';

$string['interest'] = 'Kiinnostuksen kohteet';

$string['interests'] = 'Kiinnostuksen kohteet';

$string['introduction'] = 'Esittely';

$string['jobdescription'] = 'Kuvaus työtehtävästä';

$string['jobtitle'] = 'Tehtävänimike';

$string['male'] = 'Mies';

$string['maritalstatus'] = 'Siviilisääty';

$string['membership'] = 'Ammatilliset jäsenyydet';

$string['movedown'] = 'Siirrä alas';

$string['moveup'] = 'Siirrä ylös';

$string['mygoals'] = 'Tavoitteeni';

$string['myskills'] = 'Taitoni';

$string['personalgoal'] = 'Henkilökohtaiset tavoitteet';

$string['personalinformation'] = 'Henkilötiedot';

$string['personalskill'] = 'Henkilökohtaiset taidot';

$string['placeofbirth'] = 'Syntymäpaikka';

$string['pluginname'] = 'Ansioluettelo';

$string['position'] = 'Asema';

$string['qualdescription'] = 'Kuvaus tutkinnosta';

$string['qualification'] = 'Tutkinto';

$string['qualname'] = 'Tutkinnon nimi';

$string['qualtype'] = 'Tutkintotyyppi';

$string['resume'] = 'Ansioluettelo';

$string['resumeofuser'] = '%s:n ansioluettelo';

$string['resumesaved'] = 'Ansioluettelo tallennettu';

$string['resumesavefailed'] = 'Ansioluettelon päivittäminen epäonnistui';

$string['skills'] = 'Taidot';

$string['startdate'] = 'Aloituspäivä';

$string['title'] = 'Otsikko';

$string['viewyourresume'] = 'Katso ansioluetteloasi';

$string['visastatus'] = 'Voimassaolevat viisumit';

$string['workskill'] = 'Työtaidot';

