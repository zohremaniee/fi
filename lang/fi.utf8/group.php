<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['About'] = 'Ryhmän tiedot';

$string['Admin'] = 'Pääkäyttäjä';

$string['Created'] = 'Luotu';

$string['Files'] = 'Tiedostot';

$string['Friends'] = 'Kaverit';

$string['Group'] = 'Ryhmä';

$string['Joined'] = 'Liittynyt';

$string['Members'] = 'Jäsenet';

$string['Public'] = 'Julkinen';

$string['Reply'] = 'Lähetä vastaus';

$string['Role'] = 'Rooli';

$string['Type'] = 'Tyyppi';

$string['Views'] = 'Sivut';

$string['aboutgroup'] = 'Tietoja ryhmästä %s';

$string['acceptinvitegroup'] = 'Hyväksy';

$string['addedtofriendslistmessage'] = '%s on hyväksynyt sinut kaveriksi. Tämä tarkoittaa, että %s on nyt myös kaverilistallasi. Klikkaa alla olevaa linkkiä nähdäksesi hänen profiilisivunsa';

$string['addedtofriendslistsubject'] = '%s on lisännyt sinut kaveriksi';

$string['addedtogroupmessage'] = '%s on lisännyt sinut ryhmään \'%s\'. Klikkaa alla olevaa linkkiä päästäksesi katsomaan ryhmää';

$string['addedtogroupsmessage'] = "%s on lisännyt sinut jäseneksi ryhmiin:\n\n%s\n\n";

$string['addedtogroupsubject'] = 'Sinut lisättiin ryhmään';

$string['addmembers'] = 'Lisää jäseniä';

$string['addnewinteraction'] = 'Lisää uusi %s';

$string['addtofriendslist'] = 'Lisää kaverilistalle';

$string['addtomyfriends'] = 'Lisää kaverilistalleni!';

$string['adduserfailed'] = 'Käyttäjän lisääminen epäonnistui';

$string['addusertogroup'] = 'Lisää ryhmään';

$string['allcategories'] = 'Kaikki kategoriat';

$string['allfriends'] = 'Kaikki kaverit';

$string['allgroupmembers'] = 'Kaikki ryhmän jäsenet';

$string['allgroups'] = 'Kaikki ryhmät';

$string['allmygroups'] = 'Kaikki ryhmäni';

$string['allowssubmissions'] = 'Salli palautukset';

$string['alreadyfriends'] = 'Olet jo käyttäjän %s kaveri';

$string['approve'] = 'Hyväksy';

$string['approverequest'] = 'Hyväksy pyyntö!';

$string['backtofriendslist'] = 'Paluu kaverilistaan';

$string['cannotinvitetogroup'] = 'Et voi kutsua tätä käyttäjää tähän ryhmään';

$string['cannotrequestfriendshipwithself'] = 'Et voi pyytää itseäsi kaveriksi';

$string['cantdeletegroup'] = 'Et voi poistaa tätä ryhmää';

$string['cantdenyrequest'] = 'Tämä ei ole validi kaveripyyntö';

$string['canteditdontown'] = 'Et voi muokata ryhmää, koska et omista sitä';

$string['cantleavegroup'] = 'Et voi poistaa jäsenyytäsi tästä ryhmästä';

$string['cantmessageuser'] = 'Et voi lähettää viestiä tälle käyttäjälle (Käyttäjä on määrittänyt viestiasetuksiinsa, että hän sallii viestit vain kavereilta)';

$string['cantremovefriend'] = 'Et voi poistaa tätä käyttäjää kaverilistaltasi';

$string['cantremovemember'] = "Tuutori ei voi poistaa jäseniä.";

$string['cantremoveuserisadmin'] = 'Tuutori ei voi poistaa pääkäyttäjiä eikä toisia tuutoreita.';

$string['cantrequestfriendship'] = 'Et voi lähettää kaveripyyntöä tälle käyttäjälle';

$string['cantviewmessage'] = 'Et voi katsoa tätä viestiä';

$string['categoryunassigned'] = 'Yleinen';

$string['changedgroupmembership'] = 'Ryhmän jäsenyystiedot päivitetty.';

$string['changedgroupmembershipsubject'] = 'Ryhmän jäsenyystietosi on päivitetty';

$string['changerole'] = 'Vaihda rooli';

$string['changerolefromto'] = 'Vaihda roolista %s rooliin';

$string['changeroleofuseringroup'] = 'Vaihda %s käyttäjän rooli ryhmässä %s';

$string['confirmremovefriend'] = 'Oletko varma, että haluat poistaa tämän käyttäjän kaverilistaltasi?';

$string['couldnotjoingroup'] = 'Et voi liittyä tähän ryhmään';

$string['couldnotleavegroup'] = 'Et voi poistaa jäsenyytäsi tästä ryhmästä';

$string['couldnotrequestgroup'] = 'Ei voitu lähettää ryhmäjäsenyyspyyntöä';

$string['creategroup'] = 'Luo ryhmä';

$string['current'] = "Nykyinen";

$string['currentfriends'] = 'Nykyiset kaverit';

$string['currentrole'] = 'Nykyinen rooli';

$string['declineinvitegroup'] = 'Hylkää';

$string['declinerequest'] = 'Hylkää pyyntö';

$string['declinerequestsuccess'] = 'Liittymispyyntö on evätty.';

$string['deletegroup'] = 'Ryhmä poistettu';

$string['deletegroup1'] = 'Poista ryhmä';

$string['deleteinteraction'] = 'Poista %s \'%s\'';

$string['deleteinteractionsure'] = 'Haluatko varmasti tehdä tämän? Operaatiota ei voi peruuttaa.';

$string['deletespecifiedgroup'] = 'Poista ryhmä \'%s\'';

$string['denyfriendrequest'] = 'Hylkää kaverikutsu';

$string['denyfriendrequestlower'] = 'Hylkää kaverikutsu';

$string['denyrequest'] = 'Hylkää kutsu';

$string['editgroup'] = 'Muokkaa ryhmää';

$string['editgroupmembership'] = 'Muokkaa ryhmän jäsenyystietoja';

$string['editmembershipforuser'] = 'Muokkaa käyttäjän %s jäsenyystietoja';

$string['existingfriend'] = 'nykyinen kaveri';

$string['findnewfriends'] = 'Etsi uusia kavereita';

$string['friend'] = 'kaveri';

$string['friendformacceptsuccess'] = 'Hyväksytty kaveripyyntö';

$string['friendformaddsuccess'] = '%s lisätty kaverilistallesi';

$string['friendformrejectsuccess'] = 'Hylätty kaveripyyntö';

$string['friendformremovesuccess'] = '%s poistettu kaverilistaltasi';

$string['friendformrequestsuccess'] = 'Kaveripyyntö lähetetty käyttäjälle %s';

$string['friendlistfailure'] = 'Kaverilistasi muokkaaminen epäonnistui';

$string['friendrequestacceptedmessage'] = '%s ovat hyväksyneet kaveripyyntösi ja heidät on liitetty kaverilistallesi';

$string['friendrequestacceptedsubject'] = 'Kaveripyyntö hyväksytty';

$string['friendrequestrejectedmessage'] = '%s on hylännyt kaveripyyntösi';

$string['friendrequestrejectedmessagereason'] = '%s on hylännyt kaveripyyntösi. Syy oli:';

$string['friendrequestrejectedsubject'] = 'Kaveripyyntö hylätty';

$string['friends'] = 'kaveria';

$string['friendshipalreadyrequested'] = 'Olet pyytänyt, että sinut lisätään käyttäjän %s kaverilistalle';

$string['friendshipalreadyrequestedowner'] = '%s on lähettänyt sinulle kaveripyynnön';

$string['friendshiprequested'] = 'Kaveripyyntö lähetetty!';

$string['group'] = 'ryhmä';

$string['groupadmins'] = 'Ryhmän pääkäyttäjät';

$string['groupalreadyexists'] = 'Tämän niminen ryhmä on jo olemassa';

$string['groupcategory'] = 'Ryhmäkategoria';

$string['groupconfirmdelete'] = 'Haluatko varmasti poistaa tämän ryhmän ja kaiken sen sisällön?';

$string['groupconfirmleave'] = 'Oletko varma, että haluat pois tästä ryhmästä?';

$string['groupdescription'] = 'Ryhmän kuvaus';

$string['grouphaveinvite'] = 'Sinut on kutsuttu tähän ryhmään';

$string['grouphaveinvitewithrole'] = 'Sinut on kutsuttu tähän ryhmään roolissa';

$string['groupinteractions'] = 'Ryhmän toiminta';

$string['groupinviteaccepted'] = 'Jäsenyytesi on vahvistettu! Olet nyt ryhmän jäsen.';

$string['groupinvitedeclined'] = 'Pyyntö hylätty';

$string['groupinvitesfrom'] = 'Kutsuttu liittymään ryhmään:';

$string['groupjointypecontrolled'] = 'Pääsyä tähän ryhmään on rajoitettu. Et voi liittyä ryhmään.';

$string['groupjointypeinvite'] = 'Tämän ryhmän jäseniksi tulee vain kutsutut käyttäjät';

$string['groupjointypeopen'] = 'Tämä ryhmä on avoin. Tervetuloa!';

$string['groupjointyperequest'] = 'Tämän ryhmän jäseneksi pääsee vain pyynnöstä.';

$string['groupmemberrequests'] = 'Ryhmän jäseniksi pyrkivät';

$string['groupmembershipchangedmessageaddedmember'] = 'Sinut on hyväksytty ryhmän jäseneksi';

$string['groupmembershipchangedmessageaddedtutor'] = 'Sinut on liitetty tähän ryhmään tuutorin roolissa';

$string['groupmembershipchangedmessagedeclinerequest'] = 'Jäsenpyyntöäsi tähän ryhmään ei ole hyväksytty';

$string['groupmembershipchangedmessagemember'] = 'Tässä ryhmässä sinun roolisi on muutettu tuutorista tavalliseksi jäseneksi';

$string['groupmembershipchangedmessageremove'] = 'Sinut on poistettu ryhmän jäsenlistalta';

$string['groupmembershipchangedmessagetutor'] = 'Tässä ryhmässä olet nyt tuutori';

$string['groupmembershipchangesubject'] = 'Ryhmän jäsenyys: %s';

$string['groupname'] = 'Ryhmän nimi';

$string['groupnotfound'] = 'Ryhmää id-numerolla %s ei löydy';

$string['groupnotinvited'] = 'Sinua ei ole kutsuttu liittymään tämän ryhmän jäseneksi';

$string['groupoptionsset'] = 'Ryhmän asetukset päivitetty.';

$string['grouprequestmessage'] = '%s pyytää luvan liittyä %s -ryhmän jäseneksi';

$string['grouprequestmessagereason'] = '%s haluaisi liittyä %s -ryhmäsi jäseneksi. Liittymispyynnön syy on: %s';

$string['grouprequestsent'] = 'Ryhmään liittymispyyntö lähetetty';

$string['grouprequestsubject'] = 'Uusi liittymispyyntö';

$string['groups'] = 'ryhmää';

$string['groupsaved'] = 'Ryhmä tallennettu';

$string['groupsimin'] = 'Ryhmät, joissa olen jäsenenä';

$string['groupsiminvitedto'] = 'Ryhmät, joihin minut on kutsuttu';

$string['groupsiown'] = 'Omistamani ryhmät';

$string['groupsiwanttojoin'] = 'Ryhmät, joihin haluaisin liittyä';

$string['groupsnotin'] = 'Ryhmät, joissa en ole jäsenenä';

$string['grouptype'] = 'Ryhmätyyppi';

$string['hasbeeninvitedtojoin'] = 'on kutsuttu liittymään tähän ryhmään';

$string['hasrequestedmembership'] = 'on pyytänyt jäsenyyden tähän ryhmään';

$string['interactiondeleted'] = '%s poistettu';

$string['interactionsaved'] = '%s tallennettu';

$string['invalidgroup'] = 'Ryhmä ei ole olemassa';

$string['invitationssent'] = '%d kutsua lähetetty';

$string['invite'] = 'Kutsu';

$string['invitemembertogroup'] = 'Kutsu %s liittymään \'%s\' -ryhmään';

$string['invites'] = "Kutsut";

$string['invitetogroupmessage'] = '%s on kutsunut sinut liittymään ryhmään, \'%s\'. Lisätietoja alla olevasta linkistä.';

$string['invitetogroupsubject'] = 'Sinut on kutsuttu tähän ryhmään';

$string['inviteuserfailed'] = 'Kutsu epäonnistui';

$string['inviteusertojoingroup'] = 'Kutsu';

$string['joinedgroup'] = 'Olet nyt ryhmän jäsen';

$string['joingroup'] = 'Liity tähän ryhmään';

$string['leavegroup'] = 'Poistu tästä ryhmästä';

$string['leavespecifiedgroup'] = 'Poistu ryhmästä \'%s\'';

$string['leftgroup'] = 'Olet nyt poistunut tästä ryhmästä';

$string['leftgroupfailed'] = 'Ryhmästä poistuminen epäonnistui';

$string['member'] = 'jäsen';

$string['memberchangefailed'] = 'Jäsenyyteen liittyviä tietoja ei voitu päivittää';

$string['memberchangesuccess'] = 'Jäsenyyden status muutettu';

$string['memberrequests'] = 'Liittymispyynnöt';

$string['members'] = 'jäsentä';

$string['membersdescription:controlled'] = 'Tämän ryhmän jäsenyys on kontrolloitua. Voit lisätä käyttäjiä näiden profiilisivuilta tai voit <a href="%s">lisätä useita käyttäjiä kerralla</a>.';

$string['membersdescription:invite'] = 'Tähän ryhmään voi liittyä vain kutsuttuna. Voit kutsua käyttäjiä näiden profiilisivuilta tai voit <a href="%s">lähettää useita kutsuja kerralla</a>.';

$string['membershiprequests'] = 'Liittymispyynnöt';

$string['membershiptype'] = 'Jäsenyystyyppi';

$string['membershiptype.abbrev.controlled'] = 'Kontrolloitu';

$string['membershiptype.abbrev.invite'] = 'Kutsuttuna';

$string['membershiptype.abbrev.open'] = 'Avoin';

$string['membershiptype.abbrev.request'] = 'Pyynnöstä';

$string['membershiptype.controlled'] = 'Kontrolloitu jäsenyys';

$string['membershiptype.invite'] = 'Vain kutsutuille käyttäjille';

$string['membershiptype.open'] = 'Avoin kaikille käyttäjille';

$string['membershiptype.request'] = 'Jäsenyys pyynnöstä';

$string['memberslist'] = 'Jäsenet:';

$string['messagebody'] = 'Lähetä viesti';

$string['messagenotsent'] = 'Viestin lähettäminen epäonnistui';

$string['messagesent'] = 'Viesti lähetetty';

$string['moregroups'] = 'Lisää ryhmiä';

$string['newmembersadded'] = 'Lisätty %d uutta jäsentä';

$string['newusermessage'] = 'Uusi viesti käyttäjältä %s';

$string['newusermessageemailbody'] = '%s on lähettänyt sinulle viestin. Voit lukea viestin tästä: %s';

$string['nobodyawaitsfriendapproval'] = 'Ei potentiaalisia kavereita odottamassa vastaustasi heidän kaveripyyntöönsä';

$string['nocategoryselected'] = 'Kategoriaa ei ole valittu';

$string['nogroups'] = 'Ryhmiä ei ole';

$string['nogroupsfound'] = 'Ryhmiä ei löytynyt';

$string['nointeractions'] = 'Tässä ryhmässä ei ole aktiviteetteja';

$string['nosearchresultsfound'] = 'Ei hakutuloksia :(';

$string['notallowedtodeleteinteractions'] = 'Sinulla ei ole valtuuksia poistaa tämän ryhmän aktiviteetteja';

$string['notallowedtoeditinteractions'] = 'Sinulla ei ole valtuuksia lisätä tai muokata tämän ryhmän aktiviteetteja';

$string['notamember'] = 'Et ole jäsenenä tässä ryhmässä';

$string['notinanygroups'] = 'Et ole jäsenenä missään ryhmässä';

$string['notmembermayjoin'] = 'Sinun tulee liittyä %s\'-ryhmään nähdäksesi tämän sivun.';

$string['notpublic'] = 'Tämä ryhmä ei ole julkinen.';

$string['noviewstosee'] = 'Ei mitään katsottavaa :(';

$string['pending'] = 'odotustilassa olevat pyynnöt';

$string['pendingfriends'] = 'Odotustilassa olevat kaveripyynnöt';

$string['pendingmembers'] = 'Odotustilassa olevat jäsenpyynnöt';

$string['potentialmembers'] = 'Mahdolliset jäsenet';

$string['publiclyviewablegroup'] = 'Julkinen ryhmä?';

$string['publiclyviewablegroupdescription'] = 'Salli kirjautumattomien käyttäjien nähdä ryhmä, mukaanlukien foorumit?';

$string['publiclyvisible'] = 'Julkinen';

$string['reason'] = 'Motiivi tai perustelu';

$string['reasonoptional'] = 'Perustelu (valinnainen)';

$string['reject'] = 'Hylkää';

$string['rejectfriendshipreason'] = 'Pyynnön hylkäämisen perustelu';

$string['releaseview'] = 'Palauta sivu';

$string['remove'] = 'Poista';

$string['removedfromfriendslistmessage'] = '%s on poistanut sinut kaverilistaltaan';

$string['removedfromfriendslistmessagereason'] = '%s ovat poistaneet sinut kaverilistoiltaan. Perustelu oli:';

$string['removedfromfriendslistsubject'] = 'Poistettu kaverilistalta';

$string['removedfromgroupsmessage'] = "%s on poistanut sinut ryhmistä:\n\n%s\n\n";

$string['removefriend'] = 'Poista kaveri';

$string['removefromfriends'] = 'Poista %s kaverilistalta';

$string['removefromfriendslist'] = 'Poista kaverilistalta';

$string['removefromgroup'] = 'Poista ryhmästä';

$string['request'] = 'Lähetä pyyntö';

$string['requestedfriendlistmessage'] = '%s on pyytänyt sinua lisäämään hänet kaveriksesi. Voit suorittaa operaation alla olevan linkin kautta tai kaverilistasivun kautta';

$string['requestedfriendlistmessagereason'] = '%s on pyytänyt sinua lisäämään hänet kaverikseen. Voit suorittaa operaation alla olevan linkin kautta tai kaverilistasivun kautta. Viesti: ';

$string['requestedfriendlistsubject'] = 'Uusi kaveripyyntö';

$string['requestedfriendship'] = 'Pyydetty kaveriksi';

$string['requestedmembershipin'] = 'Jäsenyys pyydetty ryhmään:';

$string['requestedtojoin'] = 'Olet pyytänyt tämän ryhmän jäsenyyttä';

$string['requestfriendship'] = 'Lähetä kaveripyyntö';

$string['requestjoingroup'] = 'Lähetä liittymispyyntö tähän ryhmään';

$string['requestjoinspecifiedgroup'] = 'Lähetä liittymispyyntö ryhmään \'%s\'';

$string['requests'] = "Pyynnöt";

$string['rolechanged'] = 'Rooli muutettu';

$string['savegroup'] = 'Tallenna ryhmä';

$string['seeallviews'] = 'Katso kaikki käyttäjän %s sivut...';

$string['sendfriendrequest'] = 'Lähetä kaveripyyntö';

$string['sendfriendshiprequest'] = 'Lähetä kaveripyyntö käyttäjälle %s';

$string['sendinvitation'] = 'Lähetä kutsu';

$string['sendinvitations'] = 'Lähetä kutsut';

$string['sendmessageto'] = 'Lähetä viesti käyttäjälle %s';

$string['submit'] = 'Lähetä';

$string['submittedviews'] = 'Arvioitavaksi lähetetyt sivut';

$string['title'] = 'Otsikko';

$string['trysearchingforfriends'] = 'Yritä %setsiä uusia kavereita%s verkostosi laajentamiseksi';

$string['trysearchingforgroups'] = 'Yritä %setsiä ryhmiä%s joihin voit liittyä';

$string['updatemembership'] = 'Päivitä jäsenyys';

$string['user'] = 'käyttäjä';

$string['useradded'] = 'Käyttäjä lisätty';

$string['useralreadyinvitedtogroup'] = 'Käyttäjä on jo kutsuttu tai on jo tämän ryhmän jäsen.';

$string['usercannotchangetothisrole'] = 'Tätä roolia käyttäjä ei voi ottaa käyttöön';

$string['usercantleavegroup'] = 'Käyttäjä ei voi poistua tästä ryhmästä';

$string['userdoesntwantfriends'] = 'Käyttäjä ei halua uusia kavereita';

$string['userinvited'] = 'Kutsu lähetetty';

$string['userremoved'] = 'Käyttäjä poistettu';

$string['users'] = 'käyttäjää';

$string['usersautoadded'] = 'Käyttäjät lisätään automaattisesti?';

$string['usersautoaddeddescription'] = 'Liitetäänkö kaikki uudet käyttäjät automaattisesti tähän ryhmään?';

$string['userstobeadded'] = 'Lisättävät käyttäjät';

$string['userstobeinvited'] = 'Kutsuttavat käyttäjät';

$string['viewmessage'] = 'Katso viesti';

$string['viewnotify'] = 'Tiedotteet jaetuista sivuista';

$string['viewnotifydescription'] = 'Jos valittu, kaikille ryhmän jäsenille lähetetään tiedote, kun uusi sivu tulee saataville. Suurissa ryhmissä tämä voi aiheuttaa paljon viestejä.';

$string['viewreleasedmessage'] = '%3$s on palauttanut ryhmään %2$s lähettämäsi sivun %1$s';

$string['viewreleasedsubject'] = 'Sivusi on palautettu';

$string['viewreleasedsuccess'] = 'Sivun palauttaminen onnistui';

$string['whymakemeyourfriend'] = 'Viesti kaveripyynnön lähettäjältä:';

$string['youaregroupadmin'] = 'Olet pääkäyttäjä tässä ryhmässä';

$string['youaregroupmember'] = 'Olet tämän ryhmän jäsen';

$string['youaregrouptutor'] = 'Olet tuutori tässä ryhmässä';

$string['youowngroup'] = 'Omistat tämän ryhmän';

