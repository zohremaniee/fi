<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['addauthority'] = 'Lisää valtuuttaja';

$string['application'] = 'Palvelu';

$string['authloginmsg'] = 'Syötä viesti, joka näkyy käyttäjille, joka kirjoittautuu palveluun Maharan kirjoittautumislomakkeen kautta';

$string['authname'] = 'Valtuuttajan nimi';

$string['cannotjumpasmasqueradeduser'] = 'You cannot jump to another application whilst masquerading as another user.' /** MISSING **/ ;

$string['cannotremove'] = 'Tunnistuspluginia ei voi poistaa, koska se on tämän instituution ainoa.';

$string['cannotremoveinuse'] = 'Tunnistuspluginia ei voi poistaa, koska se on käytössä joillain käyttäjillä. Näiden tiedot tulee päivittää, ennen kuin pluginin voi poistaa.';

$string['cantretrievekey'] = 'Julkisen avaimen hakemisessa tapahtui virhe.<br>Tarkista, että sovellus ja www-root ovat oikein määritelty ja että etäpalvelimen verkkoasetukset ovat oikein määritelty.';

$string['changepasswordurl'] = 'Salasananvaihto-osoite';

$string['duplicateremoteusername'] = 'Tämä ulkoinen käyttäjätunnus on jo käytössä käyttäjällä %s. Ulkoisten tunnusten tulee olla yksilöllisiä kussakin tunnistautumistavassa.';

$string['duplicateremoteusernameformerror'] = 'Ulkoisten tunnusten tulee olla yksilöllisiä kussakin tunnistautumistavassa.';

$string['editauthority'] = 'Muokkaa valtuuttajaa';

$string['errnoauthinstances'] = 'Tunnistuspluginia ei ole määritetty palvelimelle %s';

$string['errnoxmlrpcinstances'] = 'XMLRPC-autentikaatio ei ole käytössä palvelimelle %s';

$string['errnoxmlrpcuser'] = 'Kirjautuminen epäonnistui. Mahdollisia syitä:
* SSO-istunto on vanhentunut. Palaa toiseen palveluun ja yritä kirjautua uudelleen.
* SSO on pois käytöstä. Tarkista asia ylläpidolta.';

$string['errnoxmlrpcwwwroot'] = 'Palvelimesta %s ei ole tietoja';

$string['errorcertificateinvalidwwwroot'] = 'Sertifikaatti on luotu osoitteelle %s, mutta sitä yritetään käyttää %s:lle';

$string['errorcouldnotgeneratenewsslkey'] = 'Uuden SSL-avaimen luonti epäonnistui. Tarkista, että OpenSSL ja sen PHP-moduuli ovat asennettuina.';

$string['errornotvalidsslcertificate'] = 'Tämä SSL-sertifikaatti ei kelpaa';

$string['host'] = 'Isäntäkoneen nimi tai osoite';

$string['hostwwwrootinuse'] = 'WWW-polku on jo toisen instituution käytössä (%s)';

$string['ipaddress'] = 'IP-osoite';

$string['name'] = 'Palvelun nimi';

$string['noauthpluginconfigoptions'] = 'Tälle pluginille ei ole asetuksia';

$string['nodataforinstance'] = 'Tunnistusinstanssin tietoja ei löydy';

$string['parent'] = 'Isäntävaltuuttaja';

$string['port'] = 'Portti';

$string['protocol'] = 'Protokolla';

$string['requiredfields'] = 'Pakolliset profiilitiedot';

$string['requiredfieldsset'] = 'Pakolliset profiilitiedot asetettu';

$string['saveinstitutiondetailsfirst'] = 'Tallenna instituution tiedot ennen tunnistuspluginien asettamista.';

$string['shortname'] = 'Palvelusi lyhyt nimi';

$string['ssodirection'] = 'SSO-suunta';

$string['theyautocreateusers'] = 'Etäpalvelin luo automaattisesti käyttäjätilejä';

$string['theyssoin'] = 'Etäpalvelimelta kirjaudutaan sisään';

$string['unabletosigninviasso'] = 'SSO epäonnistui';

$string['updateuserinfoonlogin'] = 'Päivitä käyttäjän tiedot kirjautumisen yhteydessä';

$string['updateuserinfoonlogindescription'] = 'Hae ja päivitä käyttäjän tiedot etäpalvelimelta jokaisella kirjautumiskerralla.';

$string['weautocreateusers'] = 'Tämä palvelin luo automaattisesti käyttäjätilejä';

$string['weimportcontent'] = 'Sisältöä voi tuoda tälle palvelimelle';

$string['weimportcontentdescription'] = '(koskee vain joitain palveluja)';

$string['wessoout'] = 'Tältä palvelimelta kirjaudutaan ulospäin';

$string['wwwroot'] = 'WWW root';

$string['xmlrpccouldnotlogyouin'] = 'Sisäänkirjautuminen epäonnistui';

$string['xmlrpccouldnotlogyouindetail'] = 'Sisäänkirjautuminen epäonnistui, ole hyvä ja yritä uudelleen. Jos vika toistuu, ota yhteyttä ylläpitoon.';

$string['xmlrpcserverurl'] = 'XML-RPC -palvelimen osoite';

