<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Discendum Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Discendum Ltd http://discendum.com
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();


$string['cannotconnect'] = 'Cannot connect to any LDAP hosts' /** MISSING **/ ;

$string['contexts'] = 'Konteksti';

$string['description'] = 'LDAP autentiointi';

$string['distinguishedname'] = 'Edustusnimi';

$string['hosturl'] = 'Hostin URLi';

$string['ldapfieldforemail'] = 'LDAP-kenttä sähköpostille';

$string['ldapfieldforfirstname'] = 'LDAP-kenttä etunimelle';

$string['ldapfieldforsurname'] = 'LDAP-kenttä sukunimelle';

$string['ldapversion'] = 'LDAP versio';

$string['notusable'] = 'Ole hyvä ja asenna PHP LDAP laajennus';

$string['password'] = 'Salasana';

$string['searchsubcontexts'] = 'Etsi alakonteksteja';

$string['starttls'] = 'TLS salaus';

$string['title'] = 'LDAP';

$string['updateuserinfoonlogin'] = 'Päivitä käyttäjätieto kirjautumisessa';

$string['updateuserinfoonloginadnote'] = 'Huomio: tämän käyttöönotto saattaa estää joitain MS ActiveDirectoryn käyttäjiä kirjautumasta Maharaan.';

$string['userattribute'] = 'Käyttäjäominaisuus';

$string['usertype'] = 'Käyttäjätyyppi';

$string['weautocreateusers'] = 'Luomme automaattisesti käyttäjiä';

